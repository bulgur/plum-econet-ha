#!/bin/sh

version=$(tomlq .project.version pyproject.toml -rc)
requirements=$(tomlq .project.dependencies pyproject.toml -rc)
documentation=$(tomlq .project.description pyproject.toml -rc)

jq --arg version "$version" --argjson requirements "$requirements" --arg documentation "$documentation" '.version = $version | .requirements = $requirements | .documentation = $documentation' custom_components/plum_econet/manifest.json > _tmp_manifest
mv _tmp_manifest custom_components/plum_econet/manifest.json

jq --arg version "$version" --arg filename "plum-econet-ha-$version.zip" '.version = $version | .filename = $filename' hacs.json > _tmp_hacs
mv _tmp_hacs hacs.json
