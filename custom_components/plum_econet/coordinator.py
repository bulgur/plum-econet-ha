import logging
import async_timeout
from datetime import timedelta

from homeassistant.helpers.update_coordinator import (
    DataUpdateCoordinator,
    UpdateFailed,
)

from plum_econet.exceptions import EconetHTTPException, EconetUnauthorized
from plum_econet import Smartfire

_LOGGER = logging.getLogger(__name__)


class EconetCoordinator(DataUpdateCoordinator):
    def __init__(self, hass, furnace: Smartfire):
        super().__init__(
            hass,
            _LOGGER,
            name="Econet",
            update_interval=timedelta(seconds=30),
        )
        self.furnace = furnace

    async def _async_update_data(self):
        try:
            # Note: asyncio.TimeoutError and aiohttp.ClientError are already
            # handled by the data update coordinator.
            async with async_timeout.timeout(10):
                return await self.furnace.update()
        except EconetUnauthorized as err:
            raise ConfigEntryAuthFailed from err
        except EconetHTTPException as err:
            raise UpdateFailed(f"Error communicating with API: {err}")
