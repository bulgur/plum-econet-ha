from homeassistant.helpers.update_coordinator import CoordinatorEntity
from .coordinator import EconetCoordinator
from homeassistant.helpers.entity import DeviceInfo
from .const import DOMAIN


class EconetDevice(CoordinatorEntity):
    def __init__(
        self,
        *,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        name: str,
        icon: str,
        enabled_default: bool = True,
    ) -> None:
        """Initialize the IPP entity."""
        super().__init__(coordinator)
        self._attr_name = name
        self._device_id = device_id
        self._entry_id = entry_id
        self._attr_icon = icon
        self._attr_entity_registry_enabled_default = enabled_default

    @property
    def device_info(self) -> DeviceInfo | None:
        if self._device_id is None:
            return None

        attrs = self.coordinator.furnace.attributes
        return DeviceInfo(
            identifiers={(DOMAIN, self._device_id)},
            name=attrs["controllerID"],
            model=attrs["controllerID"],
            sw_version=attrs["ecosrvSoftVer"],
        )
