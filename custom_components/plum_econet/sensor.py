"""Plum Econet"""

from __future__ import annotations

import logging
from homeassistant.components.sensor import (
    SensorDeviceClass,
    SensorEntity,
    SensorStateClass,
)
from homeassistant.const import UnitOfTemperature, PERCENTAGE, REVOLUTIONS_PER_MINUTE
from homeassistant.core import callback

from .econet_device import EconetDevice
from .coordinator import EconetCoordinator

from .const import (
    DOMAIN,
    PE_NAME,
    PE_FLOAT_PRECISION,
)

_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass, config_entry, async_add_devices):
    """Set up entry."""
    coordinator = hass.data[DOMAIN][config_entry.entry_id]
    device_id = coordinator.furnace.attributes["uid"]
    entry_id = config_entry.entry_id

    name = config_entry.data[PE_NAME]
    async_add_devices(
        [
            Sensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "alarms",
                icon="mdi:alert",
            ),
            Alarms(
                entry_id,
                device_id,
                coordinator,
                name,
                "alarms",
                icon="mdi:alert",
            ),
            TempSensor(entry_id, device_id, coordinator, name, "feeder_temperature"),
            PercentageSensor(entry_id, device_id, coordinator, name, "fuel_level"),
            PercentageSensor(
                entry_id, device_id, coordinator, name, "oxygen", icon="mdi:scent"
            ),
            PercentageSensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "fan_speed",
                icon="mdi:fan",
            ),
        ]
    )


class TempSensor(EconetDevice, SensorEntity):
    """"""

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        sensor_name: str,
        enabled_default: bool = True,
        icon: str = "mdi:thermometer",
    ) -> None:
        self.sensor_name = sensor_name
        self.sensor = getattr(coordinator.furnace, self.sensor_name)
        name = f"{prefix} {sensor_name.lower().replace("_", " ").title()}"

        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=name,
            icon=icon,
            enabled_default=enabled_default,
        )

        self._attr_unique_id = f"{device_id}_{sensor_name}"
        self._attr_native_unit_of_measurement = UnitOfTemperature.CELSIUS
        self._attr_device_class = SensorDeviceClass.TEMPERATURE
        self._attr_state_class = SensorStateClass.MEASUREMENT
        self._attr_native_value = round(self.sensor.value, PE_FLOAT_PRECISION)

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        try:
            self._attr_native_value = round(self.sensor.value, PE_FLOAT_PRECISION)
        except TypeError as exc:
            _LOGGER.warning(
                "Sensor %s reported abnormal value %s of type %s",
                self._attr_unique_id,
                str(self.sensor.value),
                type(self.sensor.value),
            )
            self._attr_native_value = round(
                float(self.sensor.value), PE_FLOAT_PRECISION
            )
        self.async_write_ha_state()


class PercentageSensor(EconetDevice, SensorEntity):
    """"""

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        sensor_name: str,
        enabled_default: bool = True,
        icon: str = "mdi:gauge",
    ) -> None:
        self.sensor_name = sensor_name
        self.sensor = getattr(coordinator.furnace, self.sensor_name)
        name = f"{prefix} {sensor_name.lower().replace("_", " ").title()}"

        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=name,
            icon=icon,
            enabled_default=enabled_default,
        )

        self._attr_unique_id = f"{device_id}_{sensor_name}"

        self._attr_native_unit_of_measurement = PERCENTAGE
        self._attr_state_class = SensorStateClass.MEASUREMENT
        self._attr_native_value = self.sensor.value

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        try:
            self._attr_native_value = round(self.sensor.value, PE_FLOAT_PRECISION)
        except TypeError as exc:
            _LOGGER.warning(
                "Sensor %s reported abnormal value %s of type %s",
                self._attr_unique_id,
                str(self.sensor.value),
                type(self.sensor.value),
            )
            self._attr_native_value = round(
                float(self.sensor.value), PE_FLOAT_PRECISION
            )

        self.async_write_ha_state()


class Sensor(EconetDevice, SensorEntity):
    """"""

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        sensor_name: str,
        enabled_default: bool = True,
        icon: str = "mdi:chart-bell-curve",
    ) -> None:
        self.sensor_name = sensor_name
        self.sensor = getattr(coordinator.furnace, self.sensor_name)
        name = f"{prefix} {sensor_name.lower().replace("_", " ").title()}"

        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=name,
            icon=icon,
            enabled_default=enabled_default,
        )

        self._attr_unique_id = f"{device_id}_{sensor_name}"
        self._attr_state_class = SensorStateClass.MEASUREMENT
        self._attr_native_value = self.sensor.value

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        try:
            self._attr_native_value = round(self.sensor.value, PE_FLOAT_PRECISION)
        except TypeError as exc:
            _LOGGER.warning(
                "Sensor %s reported abnormal value %s of type %s",
                self._attr_unique_id,
                str(self.sensor.value),
                type(self.sensor.value),
            )
            self._attr_native_value = round(
                float(self.sensor.value), PE_FLOAT_PRECISION
            )
        self.async_write_ha_state()


class Alarms(EconetDevice, SensorEntity):
    """"""

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        sensor_name: str,
        enabled_default: bool = True,
        icon: str = "mdi:chart-bell-curve",
    ) -> None:
        self.sensor_name = sensor_name
        self.sensor = getattr(coordinator.furnace, self.sensor_name)
        name = f"active alarms".title()

        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=name,
            icon=icon,
            enabled_default=enabled_default,
        )

        self._attr_unique_id = f"{device_id}_active_alarms"
        if not self.sensor.value:
            self._attr_native_value = "Clear"
        else:
            self._attr_native_value = ",".join(self.furnace.get_active_alarms())

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        if not self.sensor.value:
            self._attr_native_value = "Clear"
        else:
            self._attr_native_value = ",".join(self.furnace.get_active_alarms())
        self.async_write_ha_state()
