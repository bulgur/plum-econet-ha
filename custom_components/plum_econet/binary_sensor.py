"""Plum Econet"""

from __future__ import annotations

from homeassistant.components.binary_sensor import (
    BinarySensorDeviceClass,
    BinarySensorEntity,
)
from homeassistant.core import callback

from .econet_device import EconetDevice
from .coordinator import EconetCoordinator

from .const import DOMAIN, PE_NAME


async def async_setup_entry(hass, config_entry, async_add_devices):
    """Set up entry."""
    coordinator = hass.data[DOMAIN][config_entry.entry_id]
    device_id = coordinator.furnace.attributes["uid"]
    entry_id = config_entry.entry_id

    name = config_entry.data[PE_NAME]
    async_add_devices(
        [
            BinarySensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "caution_fuel_level",
                icon="mdi:gauge-empty",
                device_class=BinarySensorDeviceClass.PROBLEM,
            ),
            BinarySensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "circulating_pump",
                icon="mdi:pump",
                device_class=BinarySensorDeviceClass.RUNNING,
            ),
            BinarySensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "boiler_pump",
                icon="mdi:pump",
                device_class=BinarySensorDeviceClass.RUNNING,
            ),
            BinarySensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "huw_pump",
                icon="mdi:pump",
                device_class=BinarySensorDeviceClass.RUNNING,
            ),
            BinarySensor(
                entry_id,
                device_id,
                coordinator,
                name,
                "fan",
                icon="mdi:fan",
                device_class=BinarySensorDeviceClass.RUNNING,
            ),
        ]
    )


class BinarySensor(EconetDevice, BinarySensorEntity):
    """"""

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        sensor_name: str,
        enabled_default: bool = True,
        icon: str = "mdi:thermometer",
        device_class: BinarySensorDeviceClass = None,
    ) -> None:
        self.sensor_name = sensor_name
        self.sensor = getattr(coordinator.furnace, self.sensor_name)
        name = f'{prefix} {sensor_name.lower().replace("_", " ").title()}'
        print(name)

        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=name,
            icon=icon,
            enabled_default=enabled_default,
        )
        self.device_class = device_class

        self._attr_unique_id = f"{device_id}_{sensor_name}"

        self._attr_is_on = self._is_on(self.sensor.value)
        self._extra_state_attribtues = {"friendly_name": self.sensor_name}

    def _is_on(self, value) -> bool:
        if type(value) is str and value.lower() == "on":
            return True
        if type(value) is int and value != 0:
            return True
        return False

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self._attr_is_on = self._is_on(self.sensor.value)
        self.async_write_ha_state()
