"""Plum Econet"""

from __future__ import annotations

from homeassistant.components.water_heater import (
    WaterHeaterEntity,
    WaterHeaterEntityFeature,
)
from homeassistant.const import UnitOfTemperature
from homeassistant.core import callback
from homeassistant.helpers.entity import EntityDescription

from .econet_device import EconetDevice
from .coordinator import EconetCoordinator
from .const import (
    DOMAIN,
    PE_NAME,
    BOILER_STATE_ATTR_UID,
    BOILER_STATE_ATTR_ECOSRV_SOFT_VER,
    BOILER_STATE_ATTR_MODULE_PANEL_SOFT_VER,
    BOILER_STATE_ATTR_MODULEA_SOFT_VER,
    BOILER_STATE_ATTR_CONTROLLER_ID,
    BOILER_STATE_ATTR_SETTINGS_VER,
    BOILER_STATE_ATTR_FIRING_UP_COUNT,
    BOILER_STATE_ATTR_NUMBER_OF_RESETS,
)
from plum_econet import Params
import logging

_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass, config_entry, async_add_devices):
    """Set up entry."""
    coordinator = hass.data[DOMAIN][config_entry.entry_id]
    device_id = coordinator.furnace.attributes["uid"]
    entry_id = config_entry.entry_id

    name = config_entry.data[PE_NAME]
    devices = [
        EconetBoiler(entry_id, device_id, coordinator, name, "mdi:water-boiler"),
        EconetHUW(entry_id, device_id, coordinator, name, "mdi:water-boiler"),
    ]
    for mid, mixer in enumerate(coordinator.furnace.mixers):
        devices.append(
            EconetMixer(
                entry_id, device_id, coordinator, name, "mdi:water-boiler", num=mid
            )
        )
    async_add_devices(devices)


class EconetBoiler(EconetDevice, WaterHeaterEntity):
    _attr_supported_features = WaterHeaterEntityFeature.TARGET_TEMPERATURE
    _attr_temperature_unit = UnitOfTemperature.CELSIUS

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        icon: str,
        enabled_default: bool = True,
    ) -> None:
        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=f"{prefix} Boiler",
            icon=icon,
            enabled_default=enabled_default,
        )
        self._fallback_icon = icon
        self._update_icon()
        self._attr_unique_id = (
            f"{device_id}_{coordinator.furnace.attributes['controllerID']}"
        )
        self._extra_state_attributes = self._set_extra_attrs()

    @property
    def min_temp(self):
        return self.coordinator.furnace.boiler.target_temperature_min

    @property
    def max_temp(self):
        return self.coordinator.furnace.boiler.target_temperature_max

    @property
    def current_temperature(self):
        return self.coordinator.furnace.boiler.temperature

    @property
    def current_operation(self):
        return self.coordinator.furnace.operation_mode

    @property
    def target_temperature(self):
        return self.coordinator.furnace.boiler.target_temperature

    async def async_set_temperature(self, **kwargs: Any) -> None:
        await self.coordinator.furnace.boiler.set_target_temperature(
            kwargs["temperature"]
        )

    @property
    def extra_state_attributes(self):
        return self._extra_state_attributes

    def _update_icon(self):
        MODE_ICONS = {
            0: "mdi:fire-off",
            1: "mdi:gas-burner",
            2: "mdi:fire-circle",
            3: "mdi:fire",
            4: "mdi:tower-fire",
            5: "mdi:smoke",
            6: "mdi:cog-stop-outline",
            7: "mdi:sprinkler-fire",
            8: "mdi:hand-wave",
            9: "mdi:fire-alert",
            10: "mdi:door-open",
            11: "mdi:smoke-detector-variant-alert",
            12: "mdi:cog-refresh-outline",
            13: "mdi:cog-off-outline",
        }
        self.icon = MODE_ICONS.get(
            self.coordinator.furnace.operation_mode.raw, self._fallback_icon
        )

    def _set_extra_attrs(self):
        attrs = self.coordinator.furnace.attributes
        furnace = self.coordinator.furnace
        return {
            BOILER_STATE_ATTR_UID: attrs["uid"],
            BOILER_STATE_ATTR_ECOSRV_SOFT_VER: attrs["ecosrvSoftVer"],
            BOILER_STATE_ATTR_MODULE_PANEL_SOFT_VER: attrs["modulePanelSoftVer"],
            BOILER_STATE_ATTR_MODULEA_SOFT_VER: attrs["moduleASoftVer"],
            BOILER_STATE_ATTR_CONTROLLER_ID: attrs["controllerID"],
            BOILER_STATE_ATTR_SETTINGS_VER: attrs["settingsVer"],
            BOILER_STATE_ATTR_FIRING_UP_COUNT: furnace.econet.get_param(
                Params.FIRING_UP_COUNT
            ).value,
            BOILER_STATE_ATTR_NUMBER_OF_RESETS: furnace.econet.get_param(
                Params.NUMBER_OF_RESETS
            ).value,
        }

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self._extra_state_attributes = self._set_extra_attrs()
        self._update_icon()
        self.async_write_ha_state()


class EconetHUW(EconetDevice, WaterHeaterEntity):
    _attr_supported_features = WaterHeaterEntityFeature.TARGET_TEMPERATURE
    _attr_temperature_unit = UnitOfTemperature.CELSIUS

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        icon: str,
        enabled_default: bool = True,
    ) -> None:
        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=f"{prefix} HUW",
            icon=icon,
            enabled_default=enabled_default,
        )
        self._attr_unique_id = (
            f"{device_id}_{coordinator.furnace.attributes['controllerID']}_huw"
        )

    @property
    def ax_temp(self):
        return self.coordinator.furnace.huw.target_temperature_min

    @property
    def max_temp(self):
        return self.coordinator.furnace.huw.target_temperature_max

    @property
    def current_temperature(self):
        return self.coordinator.furnace.huw.temperature

    @property
    def current_operation(self):
        return self.coordinator.furnace.huw_pump

    @property
    def target_temperature(self):
        return self.coordinator.furnace.huw.target_temperature

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self.async_write_ha_state()


class EconetMixer(EconetDevice, WaterHeaterEntity):
    _attr_supported_features = WaterHeaterEntityFeature.TARGET_TEMPERATURE
    _attr_temperature_unit = UnitOfTemperature.CELSIUS

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        icon: str,
        enabled_default: bool = True,
        num: int = 1,
    ) -> None:
        self.entity_description = EntityDescription(
            key=f"mixer_{num+1}", name=f"Mixer_{num+1}"
        )
        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=f"{prefix} Mixer{num+1}",
            icon=icon,
            enabled_default=enabled_default,
        )
        self.mixer = coordinator.furnace.mixers[num]
        self._attr_unique_id = f"{device_id}_{coordinator.furnace.attributes['controllerID']}_mixer_{num+1}"

    @property
    def min_temp(self):
        return self.mixer.target_temperature_min

    @property
    def max_temp(self):
        return self.mixer.target_temperature_max

    @property
    def current_temperature(self):
        return self.mixer.temperature

    @property
    def current_operation(self):
        return "on" if self.mixer.pump else "off"

    @property
    def target_temperature(self):
        return self.mixer.target_temperature

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self.async_write_ha_state()
