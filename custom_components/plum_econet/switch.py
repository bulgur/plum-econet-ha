"""Plum Econet"""

from __future__ import annotations

import logging
from homeassistant.components.switch import (
    SwitchEntity,
)
from homeassistant.core import callback

from .econet_device import EconetDevice
from .coordinator import EconetCoordinator

from typing import Any
from .const import (
    DOMAIN,
    PE_NAME,
)

_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass, config_entry, async_add_devices):
    """Set up entry."""
    coordinator = hass.data[DOMAIN][config_entry.entry_id]
    device_id = coordinator.furnace.attributes["uid"]
    entry_id = config_entry.entry_id

    name = config_entry.data[PE_NAME]
    async_add_devices(
        [
            Switch(
                entry_id,
                device_id,
                coordinator,
                name,
                "summer_mode",
                icon="mdi:weather-sunny",
            ),
        ]
    )


class Switch(EconetDevice, SwitchEntity):
    """"""

    def __init__(
        self,
        entry_id: str,
        device_id: str,
        coordinator: EconetCoordinator,
        prefix: str,
        setting_name: str,
        enabled_default: bool = True,
        icon: str = "mdi:thermometer",
    ) -> None:
        self.setting_name = setting_name
        self.setting = getattr(coordinator.furnace, self.setting_name)
        name = f'{prefix} {setting_name.lower().replace("_", " ").title()}'
        print(name)

        super().__init__(
            entry_id=entry_id,
            device_id=device_id,
            coordinator=coordinator,
            name=name,
            icon=icon,
            enabled_default=enabled_default,
        )
        self._attr_unique_id = f"{device_id}_{setting_name}"

        self._attr_is_on = self._is_on(self.setting.value)
        self._extra_state_attribtues = {"friendly_name": self.setting_name}

    async def async_turn_on(self, **kwargs: Any) -> None:
        await self.setting.set_value(1)

    async def async_turn_off(self, **kwargs: Any) -> None:
        await self.setting.set_value(0)

    def _is_on(self, value) -> bool:
        if type(value) is str and value.lower() == "on":
            return True
        if type(value) is int and value != 0:
            return True
        return False

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self._attr_is_on = self._is_on(self.setting.value)
        self.async_write_ha_state()
