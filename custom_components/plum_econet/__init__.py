"""The Plum Econet integration."""

from __future__ import annotations
import logging

from homeassistant.const import Platform
from homeassistant.core import HomeAssistant

from plum_econet import Smartfire
from .coordinator import EconetCoordinator

from .const import DOMAIN, PE_USERNAME, PE_PASSWORD, PE_URL

PLATFORMS: list(str) = [
    Platform.SWITCH,
    Platform.BINARY_SENSOR,
    Platform.SENSOR,
    Platform.WATER_HEATER,
]
_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:
    """Set up Plum Econet from a config entry."""
    furnace = Smartfire(
        entry.data[PE_URL], entry.data[PE_USERNAME], entry.data[PE_PASSWORD]
    )
    await furnace.update()
    coordinator = EconetCoordinator(hass, furnace)
    await coordinator.async_config_entry_first_refresh()
    hass.data.setdefault(DOMAIN, {})[entry.entry_id] = coordinator
    await hass.config_entries.async_forward_entry_setups(entry, PLATFORMS)
    return True


async def async_unload_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:
    """Unload a config entry."""
    if unload_ok := await hass.config_entries.async_unload_platforms(entry, PLATFORMS):
        hass.data[DOMAIN].pop(entry.entry_id)

    return unload_ok
