"""Constants for the Plum Econet integration."""

DOMAIN = "plum_econet"

PE_USERNAME = "username"
PE_URL = "url"
PE_PASSWORD = "password"
PE_NAME = "name"

BOILER_STATE_ATTR_UID = "uid"
BOILER_STATE_ATTR_ECOSRV_SOFT_VER = "ecosrvSoftVer"
BOILER_STATE_ATTR_MODULE_PANEL_SOFT_VER = "modulePanelSoftVer"
BOILER_STATE_ATTR_MODULEA_SOFT_VER = "moduleASoftVer"
BOILER_STATE_ATTR_CONTROLLER_ID = "controllerID"
BOILER_STATE_ATTR_SETTINGS_VER = "settingsVer"
BOILER_STATE_ATTR_FIRING_UP_COUNT = "firingUpCount"
BOILER_STATE_ATTR_NUMBER_OF_RESETS = "numberOfResets"

PE_FLOAT_PRECISION = 3
