"""Config flow for Plum Econet integration."""

from __future__ import annotations

import logging
from typing import Any

import voluptuous as vol

from homeassistant import config_entries
from homeassistant.core import HomeAssistant
from homeassistant.data_entry_flow import FlowResult
from homeassistant.exceptions import HomeAssistantError
from plum_econet import Smartfire
from plum_econet.exceptions import EconetHTTPException, EconetUnauthorized
from homeassistant.helpers import config_validation as cv

from .const import DOMAIN, PE_PASSWORD, PE_USERNAME, PE_URL, PE_NAME

_LOGGER = logging.getLogger(__name__)

STEP_USER_DATA_SCHEMA = vol.Schema(
    {
        vol.Required(PE_NAME): str,
        vol.Required(PE_URL): str,
        vol.Required(PE_USERNAME): str,
        vol.Required(PE_PASSWORD): str,
    }
)


async def validate_api_auth(furnace: Smartfire) -> bool:
    """Test if we can authenticate with the url."""
    try:
        await furnace.update()
        assert furnace.econet.params is not None
    except EconetHTTPException:
        return CannotConnect
    except EconetUnauthorized:
        raise InvalidAuth

    return True


async def validate_input(hass: HomeAssistant, data: dict[str, Any]) -> dict[str, Any]:
    """Validate the user input allows us to connect.

    Data has the keys from STEP_USER_DATA_SCHEMA with values provided by the user.
    """

    try:
        vol.Schema(vol.Url())(data[PE_URL])
    except vol.error.MultipleInvalid:
        raise InvalidUrl

    furnace = Smartfire(data[PE_URL], data[PE_USERNAME], data[PE_PASSWORD])

    if not await validate_api_auth(furnace):
        raise InvalidAuth

    # Return info that you want to store in the config entry.
    return {"device_id": furnace.attributes["uid"]}


class ConfigFlow(config_entries.ConfigFlow, domain=DOMAIN):
    """Handle a config flow for Plum Econet."""

    VERSION = 1

    async def async_step_user(
        self, user_input: dict[str, Any] | None = None
    ) -> FlowResult:
        """Handle the initial step."""
        errors = {}

        if user_input is not None:
            try:
                info = await validate_input(self.hass, user_input)
                await self.async_set_unique_id(info["device_id"])
                self._abort_if_unique_id_configured()
            except InvalidUrl:
                errors["url"] = "invalid_url"
            except CannotConnect:
                errors["base"] = "cannot_connect"
            except InvalidAuth:
                errors["base"] = "invalid_auth"
            except Exception:  # pylint: disable=broad-except
                _LOGGER.exception("Unexpected exception")
                errors["base"] = "unknown"
            else:
                return self.async_create_entry(
                    title=user_input[PE_NAME], data=user_input
                )

        return self.async_show_form(
            step_id="user", data_schema=STEP_USER_DATA_SCHEMA, errors=errors
        )


class CannotConnect(HomeAssistantError):
    """Error to indicate we cannot connect."""


class InvalidAuth(HomeAssistantError):
    """Error to indicate there is invalid auth."""


class InvalidUrl(HomeAssistantError):
    """Error to indicate there is invalid auth."""
